const express = require('express');
const Track = require('../models/Track');

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        if(req.query.album){
            Track.find({album: req.query.album}).populate('Album')
                .then(response => res.send(response))
                .catch(() => res.sendStatus(400));
        } else {
            Track.find().populate('Album')
                .then(tracks => res.send(tracks))
                .catch(error => res.status(404).send(error));
        }
    });
    router.post('/', (req, res) => {
        const track = new Track(req.body);
        track.save()
            .then(track => res.send(track))
            .catch((error) => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;