const express = require('express');
const Trackhistory = require('../models/Trackhistory');
const User = require('../models/User');
const date = new Date();

const createRouter = () => {
    const router = express.Router();

    router.post('/', async (req, res) => {
        const token = req.get('token');
        const user = await User.findOne({token});
        if (user) {
            const trackhistory = new Trackhistory();
            trackhistory.datetime = date.toISOString();
            trackhistory.user = user._id;
            trackhistory.track = req.body.track;
            trackhistory.save()
                .then(result => res.send(result))
                .catch(err => res.status(404).send('This is ' + err));
        }
        else res.sendStatus(401);
    });

    return router;
};

module.exports = createRouter;